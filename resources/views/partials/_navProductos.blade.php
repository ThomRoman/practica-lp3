<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container container-fluid">
      <a class="navbar-brand" href="#">Productos</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link {{ Request::getPathInfo() != '/productos' ?:'active' }}" aria-current="page" href="{{ route('productos.index') }}">Mostrar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ Request::getPathInfo() != '/productos/agregar' ?:'active' }}" aria-current="page" href="{{ route('productos.agregar') }}">Agregar</a>
          </li>

        </ul>
        <li class="nav-item dropdown" style="list-style: none">
            <a class="nav-link dropdown-toggle" href="#" style="color: white" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fas fa-cogs"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="/">Home</a></li>
              <li><a class="dropdown-item" href="{{ route('empleados.index') }}">Empleados</a></li>
            </ul>
          </li>
      </div>
    </div>
  </nav>

