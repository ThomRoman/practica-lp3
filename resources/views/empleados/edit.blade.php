
@extends('layouts.base')
@section('title',"Empleados")
@section('nav')
    @include('partials._navEmpleados')
@endsection
@section('content')
<div class="card p-5 pt-1 w-50 m-auto">


    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <h1 class="text-center">Editar Empleado</h1>
        </div>
    </div>
    <form method="POST" action="{{ route('empleados.update',['empleado'=>$empleado]) }}"enctype="multipart/form-data">
        {{-- @csrf --}}
        @method('PUT')
        {{ csrf_field() }}
        <div class="form-group">
            <label for="formGroupExampleInput">Nombre</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="nombre" name="nombreE" required value="{{ $empleado->nombreE }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Foto</label>
            <br>
            <input type="file" name="foto" id="foto" value="">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Editar</button>
    </form>
</div>
@endsection
