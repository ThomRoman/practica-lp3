
@extends('layouts.base')
@section('title',"Empleados")
@section('nav')
    @include('partials._navEmpleados')
@endsection
@section('content')
<table class="table w-50 m-auto">
    <thead class="table">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nombre</th>
        <th scope="col">Foto</th>
        <th scope="col">acciones</th>
      </tr>
    </thead>
    <tbody>
    @foreach($empleados as $empleado)
        <tr>
            <td> {{ $empleado->id }}</td>
            <td> {{ $empleado->nombreE }}</td>
            <td><img src="{{ asset('storage/'.$empleado->foto) }}" alt="" style="height: 80px"></td>

            <td>
                <a type="button" class="btn btn-danger ml-auto" type="submit" href="{{ route('empleados.delete',['empleado'=>$empleado]) }}"><i class="far fa-trash-alt"></i></a>
                <a type="button" class="btn btn-primary" style="" href="{{ route('empleados.editar',['empleado'=>$empleado]) }}"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
    @endforeach

    </tbody>
  </table>
@endsection
