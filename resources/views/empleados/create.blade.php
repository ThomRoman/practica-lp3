
@extends('layouts.base')
@section('title',"Empleados")
@section('nav')
    @include('partials._navEmpleados')
@endsection
@section('content')
<div class="card p-5 pt-1 w-50 m-auto">


    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <h1 class="text-center">Crear Empleados</h1>
        </div>
    </div>
    <form method="POST" action="{{ route('empleados.store') }}" enctype="multipart/form-data">
        {{-- @csrf --}}
        {{ csrf_field() }}
        <div class="form-group">
            <label for="formGroupExampleInput">Nombre</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="nombre" name="nombreE" required>
        </div>
        <br>
        <div class="form-group">
            <label for="formGroupExampleInput">Foto</label>
            <br>
            <input type="file" name="foto" id="foto" value="">
        </div>
        <button type="submit" class="btn btn-primary btn-block mt-4 w-100">Crear</button>
    </form>
</div>
@endsection
