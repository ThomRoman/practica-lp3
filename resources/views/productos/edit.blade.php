
@extends('layouts.base')
@section('title',"Productos")
@section('nav')
    @include('partials._navProductos')
@endsection
@section('content')
<div class="card p-5 pt-1 w-50 m-auto">


    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <h1 class="text-center">Editar Producto</h1>
        </div>
    </div>
    <form method="POST" action="{{ route('productos.update',['producto'=>$producto]) }}"enctype="multipart/form-data">
        {{-- @csrf --}}
        @method('PUT')
        {{ csrf_field() }}
        <div class="form-group">
            <label for="formGroupExampleInput">Nombre</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="nombre" name="nomb_producto" required value="{{ $producto->nomb_producto }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Marca</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="marca" name="marca_id" required value="{{ $producto->marca_id }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Categoria</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="marca" name="categoria_id" required value="{{ $producto->categoria_id }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Precio</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="precio" name="precio_producto" required value="{{ $producto->precio_producto }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Descripcion</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="descripcion" name="desc_producto" required>{{ $producto->desc_producto }}</textarea>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">cantidad</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="cantidad" name="caract_producto" required value="{{ $producto->caract_producto }}">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Foto</label>
            <br>
            <input type="file" name="img_producto" id="foto" value="">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Editar</button>
    </form>
</div>
@endsection
