
@extends('layouts.base')
@section('title',"Productos")
@section('nav')
    @include('partials._navProductos')
@endsection
@section('content')
<div class="card p-5 pt-1 w-50 m-auto">


    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <h1 class="text-center">Crear Productos</h1>
        </div>
    </div>
    <form method="POST" action="{{ route('productos.store') }}" enctype="multipart/form-data">
        {{-- @csrf --}}
        {{ csrf_field() }}
        <div class="form-group">
            <label for="formGroupExampleInput">Nombre</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="nombre" name="nomb_producto" required>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Marca ID</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="marca" name="marca_id" required>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Categoria ID</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="marca" name="categoria_id" required>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Precio</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="precio" name="precio_producto" required>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Descripcion</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="descripcion" name="desc_producto" required></textarea>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Caracteristica</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="cantidad" name="caract_producto" required>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Foto</label>
            <br>
            <input type="file" name="img_producto" id="foto" value="">
        </div>
        <button type="submit" class="btn btn-primary btn-block mt-4 w-100">Crear</button>
    </form>
</div>
@endsection
