
@extends('layouts.base')
@section('title',"Productos")
@section('nav')
    @include('partials._navProductos')
@endsection
@section('content')
<table class="table w-75 m-auto">
    <thead class="table">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nombre</th>
        <th scope="col">marca id</th>
        <th scope="col">categoria id</th>
        <th scope="col">precio</th>
        <th scope="col">descripcion</th>
        <th scope="col">caracteristica</th>
        <th scope="col">foto</th>
        <th scope="col">acciones</th>
      </tr>
    </thead>
    <tbody>
    @foreach($productos as $producto)
        <tr>
            <td> {{ $producto->id }}</td>
            <td> {{ $producto->nomb_producto }}</td>
            <td> {{ $producto->marca_id }}</td>
            <td> {{ $producto->categoria_id }}</td>
            <td> {{ $producto->precio_producto }}</td>
            <td> {{ $producto->desc_producto }}</td>
            <td> {{ $producto->caract_producto }}</td>
            <td><img src="{{ asset('storage/'.$producto->img_producto) }}" alt="" style="height: 80px"></td>
            <td>
                <a type="button" class="btn btn-danger ml-auto" type="submit" href="{{ route('productos.delete',['producto'=>$producto]) }}"><i class="far fa-trash-alt"></i></a>
                <a type="button" class="btn bg-primary" style="color: white"  href="{{ route('productos.editar',['producto'=>$producto]) }}"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
    @endforeach

    </tbody>
  </table>
@endsection
