<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string("nomb_producto");

            $table->double("precio_producto");
            $table->text("desc_producto");
            $table->text("caract_producto");
            $table->string("img_producto");
            // $table->integer('marca_id')->unsigned();
            $table->unsignedBigInteger('marca_id');
            $table->unsignedBigInteger('categoria_id');
            // $table->integer('categoria_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
