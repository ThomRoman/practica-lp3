<?php

namespace App\Http\Controllers;

use App\Models\Empleados;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empleados = Empleados::all();
        return view('empleados.index',['empleados'=>$empleados]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosEmpleado=request()->except('_token');

        if ($request->hasfile('foto')){
            $datosEmpleado['foto']=$request->file('foto')->store('uploads','public');
        }
        Empleados::insert($datosEmpleado);

        $empleados = Empleados::all();
        return redirect()->route('empleados.index')->with('empleados',$empleados);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(Empleados $empleados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleados $empleados)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleados $empleados)
    {
        //
        $datosEmpleado=request()->except('_token');

        if ($request->hasfile('foto')){
            $datosEmpleado['foto']=$request->file('foto')->store('uploads','public');
        }
        $empleadoActual = Empleados::find($request->empleado);
        $empleadoActual->nombreE = $request->nombreE;
        $empleadoActual->foto = $datosEmpleado['foto'];
        $empleadoActual->save();
        // return view('empleados.index',['empleados'=>Empleados::all()]);
        return redirect()->route('empleados.index')->with('empleados',Empleados::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleados $empleados)
    {

        dd($empleados);

        // return redirect()->route('empleados.index');

    }



}
