<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productos = Productos::all();
        // $categorias =

        return view('productos.index',['productos'=>$productos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosProductos=request()->except('_token');

        if ($request->hasfile('img_producto')){
            $datosProductos['img_producto']=$request->file('img_producto')->store('uploads','public');
        }
        $datosProductos['precio_producto'] = (double) $request->precio_producto;
        Productos::insert($datosProductos);

        return redirect()->route('productos.index')->with('productos',Productos::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $productos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productos $productos)
    {
        //
        $datosProductos=request()->except('_token');

        if ($request->hasfile('img_producto')){
            $datosProductos['img_producto']=$request->file('img_producto')->store('uploads','public');
        }
        $productoActual = Productos::find($request->producto);
        $productoActual->nomb_producto = $request->nomb_producto;
        $productoActual->marca_id = $request->marca_id;
        $productoActual->categoria_id = $request->categoria_id;
        $productoActual->precio_producto = (double)$request->precio_producto;
        $productoActual->desc_producto = $request->desc_producto;
        $productoActual->caract_producto = $request->caract_producto;
        $productoActual->img_producto = $datosProductos['img_producto'];
        $productoActual->save();
        return redirect()->route('productos.index')->with('productos',Productos::all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $productos)
    {
        //
    }
}
