<?php

use App\Http\Controllers\EmpleadosController;
use App\Http\Controllers\ProductosController;
use App\Models\Empleados;
use App\Models\Productos;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// los customizados deben ir antes de los resources

// Empleados

Route::get('/empleados/agregar',function(){
    return view('empleados.create');
})->name('empleados.agregar');

Route::get('/empleados/delete/{empleado}',function(Empleados $empleado){
    $empleado->delete();
    return redirect()->route('empleados.index')->with('empleados',Empleados::all());
})->name("empleados.delete");

Route::get('/empleados/editar/{empleado}',function(Empleados $empleado){
    return view("empleados.edit",["empleado"=>$empleado]);
})->name('empleados.editar');


// Productos
Route::get('/productos/agregar',function(){
    return view('productos.create');
})->name('productos.agregar');

Route::get('/productos/delete/{producto}',function(Productos $producto){
    $producto->delete();
    return redirect()->route('productos.index')->with('productos',Productos::all());
})->name("productos.delete");

Route::get('/productos/editar/{producto}',function(Productos $producto){
    return view("productos.edit",["producto"=>$producto]);
})->name('productos.editar');

Route::resource('empleados',EmpleadosController::class);
Route::resource('productos',ProductosController::class);
